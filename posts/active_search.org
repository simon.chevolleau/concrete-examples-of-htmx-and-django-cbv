#+TITLE: Active search: ListView filter
#+DATE: <2023-11-12>
#+OPTIONS: toc:nil num:nil
#+OPTIONS: ^:nil


* Intro
Here is a small tutorial on how to set up an active search by a ListView based on user typing.
* Model: Game
#+begin_src python
from django.db import models

class Game(models.Model):
    class Meta:
        ordering = ["french_name"]

    bgg_pk = models.CharField(max_length=200, null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    french_name = models.CharField(default="",
                                   max_length=600,
                                   null=True,
                                   blank=True)
#+end_src

* Views: SearchView
By looking at the get request, I'm able to know if the request is from HTMX or not. This means, I can render one of my partial templates inside my already rendered HTML page from "" URL entry. Also, if user is using directly "detail/" or "update/" URLs (instead of HTMX using them). It will render an HTML page with the general template + the corresponding partial template. In consequence, these URLs are still valid even if they're not used by HTMX request.
By default, partial template is set on detail, this is changed using "update/" URL.

#+begin_src python
from django.views.generic import DetailView, ListView
from game.models import Game

class SearchView(ListView):
    model = Game
    template_name = "home.html"
    context_object_name = "games"
    result_template = "search_results.html"

    def get(self, request, *args, **kwargs):
        if request.META.get("HTTP_HX_REQUEST"):
            self.template_name = self.result_template
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        query = self.request.GET.get("search", "")
        return Game.objects.filter(name__icontains=query)
#+end_src

* URLs
"update/" URL pass a new value to the partial_template attribute.
#+begin_src python
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from game.views import SearchView

urlpatterns = [
    path("", SearchView.as_view(), name="home"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#+end_src
* Templates
** home.html
#+begin_src html
{% extends "base.html" %}
{% block title %}Accueil{% endblock title %}

{% block content %}
  {% include "header/header.html" %}

  <section class="game-list" id="search-results">
    {% include "search_results.html" %}
  </section>
{% endblock content %}
#+end_src
** search_results.html
#+begin_src html
{% for game in games %}
  <div class="game">
    <a href = "{% url 'game_description' game.id %}">
    <h2>{{ game.french_name }}</h2>
      <p class = "booking.game_week_price">{{ game.week_price }}€</p>
      {% if game.image %}
        <img class="game_img" src="{{ game.image.url }}">
      {% endif %}
    </a>
  </div>
{% endfor %}
#+end_src
** header.html
#+begin_src html
{% extends "base.html" %}

{% block title %}Header{% endblock title %}

{% block content %}

<header>
  {% include "header/search_bar.html" %}
</header>

{% endblock content %}
#+end_src
** search_bar.html
#+begin_src html
<form method="GET" action="{% url 'home' %}">
  {% csrf_token %}
  <input class="form-control" type="search"
         name="search" placeholder="Trouver votre jeux ..."
         hx-get=""
         hx-trigger="keyup changed, search"
         hx-target="#search-results"
         hx-swap="innerHTML">
</form>
#+end_src
