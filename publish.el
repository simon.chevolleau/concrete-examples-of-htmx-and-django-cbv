;;; publish.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Simon Chevolleau
;;
;; Author: Simon Chevolleau <simon.chevolleau@gmail.com>
;; Maintainer: Simon Chevolleau <simon.chevolleau@gmail.com>
;; Created: October 28, 2023
;; Modified: October 28, 2023
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/simon/publish
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
(add-to-list 'load-path "/home/simon/.config/emacs/.local/straight/build-29.1/htmlize/")
(add-to-list 'load-path "/home/simon/.config/emacs/.local/straight/build-29.1/weblorg/")
(add-to-list 'load-path "/home/simon/.config/emacs/.local/straight/build-29.1/templatel/")
(require 'htmlize)
(require 'weblorg)
(require 'templatel)

;; defaults to http://localhost:8000
(setq weblorg-default-url "")
;; (if (string= (getenv "ENV") "prod")
;;  (setq weblorg-default-url "https://emacs.love/weblorg"))
;; (print weblorg-default-url)
(setq weblorg-default-title "Concrete examples of HTMX and Django CBV")
(setq weblorg-default-author "Simon Chevolleau")
(setq weblorg-default-email "simon.chevolleau@gmail.com")
(setq weblorg-default-description "Concrete examples of HTMX and Django CBV")
(setq weblorg-default-keywords "htmx, django, cbv, class based views, concrete examples, examples, code, code snippets, code examples,")
(setq weblorg-default-lang "en")
(setq weblorg-default-theme "default")
(setq weblorg-default-date-format "%Y-%m-%d")
(setq weblorg-default-time-format "%H:%M:%S")
(setq weblorg-default-timezone "Europe/Paris")
(setq weblorg-default-summary "Concrete examples of HTMX and Django CBV")
;; Set footer
(setq weblorg-default-footer "© 2021 Simon Chevolleau.")
;; Change output directory to public
(setq weblorg-default-output-directory "public")


(weblorg-route
 :name "posts"
 :input-pattern "posts/*.org"
 :template "post.html"
 :output "output/posts/{{ slug }}.html"
 :url "/posts/{{ slug }}.html")

;; Generate pages
(weblorg-route
 :name "pages"
 :input-pattern "pages/*.org"
 :template "page.html"
 :output "output/{{ slug }}/index.html"
 :url "/{{ slug }}")

;; Generate posts summary
(weblorg-route
 :name "index"
 :input-pattern "posts/*.org"
 :input-aggregate #'weblorg-input-aggregate-all-desc
 :template "blog.html"
 :output "output/index.html"
 :url "/")

(weblorg-copy-static
 :output "output/static/{{ file }}"
 :url "/static/{{ file }}")

(weblorg-export)
